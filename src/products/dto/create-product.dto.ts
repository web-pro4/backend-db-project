import { IsNotEmpty, Length, Matches,  } from "class-validator";


export class CreateProductDto {
  @IsNotEmpty()
  @Length(4,16)
  name: string;

  @IsNotEmpty()
  @Matches(/^(?!(?:0|0\.0|0\.00)$)[+]?\d+(\.\d|\.\d[0-9])?$/  )
  price : number;


}